export default class NodeDomain {

    constructor(data, index, total)
    {
        this.index = index
        this.total = total
        this.type = data.type ? data.type : 0
        this.label = data.label ? data.label : ''
        this.url = data.url ? data.url : ''
        this.className = 'menu-item ' + (this.type === 0 ? 'folder' : 'picture')
        this.readStyle()
    }

    readStyle()
    {
        let gap = 80 + (this.total <=4 ? 0 : 40 * (this.total - 4))
        let translateX = (-this.total - gap)
        let rotate = (90 / this.total * this.index) + 182
        this.style = {transform: `rotate(${rotate}deg) translateX(${translateX}px)`}
    }

    get toHtml()
    {
        let dto = {}
        dto.key = this.label
        dto.className = this.className
        dto.style = this.style
        return dto
    }

    get toPictureHtml()
    {
        let dto = {}
        dto.key = this.label
        dto.href = '#'
        dto.className = 'photo'
        dto.style = {backgroundImage: `url(${this.url})`}
        return dto
    }
}