import {loadNodes} from '../actions/actionCreator'
import NodeDomain from '../domain/NodeDomain'
//import { browserHistory } from 'react-router'

export default class Api {
  static loadNodes(node){
    return dispatch => {
      const requestInfo = {
        method:'GET',
        headers: new Headers({
          'X-TOKEN':'2d4e69f4823176197ccf41caa5ee6456'
        })
      };
      fetch(`http://34.240.128.157/public/explorePictures?path=${node}`, requestInfo)
      .then(response => response.json())
      .then(nodes => {
        nodes = nodes.data.children
        dispatch(loadNodes(nodes.map((node, index) => new NodeDomain(node, index, nodes.length))))
        return nodes
      })
      .catch(error => {
        //alert('Error on nodes - Moving back to root')
        //browserHistory.push('/home');
        //return null;
        alert('Emulating data')
        let nodes = [
          {
            "type": 0,
            "label": "folder0_0_0"
          },
          {
            "type": 0,
            "label": "folder0_0_1"
          },
          {
            "type": 0,
            "label": "folder0_0_2"
          },
          {
            "type": 0,
            "label": "folder0_0_3"
          },
          {
            "type": 0,
            "label": "folder0_0_4"
          },
          {
            "type": 0,
            "label": "folder0_0_5"
          },
          {
            "type": 1,
            "label": "picture0_0_6",
            "url": "http://thecatapi.com/api/images/get?image_id=999"
          },
          {
            "type": 1,
            "label": "picture1_0_7",
            "url": "http://thecatapi.com/api/images/get?image_id=811"
          },
          {
            "type": 1,
            "label": "picture2_0_7",
            "url": "http://thecatapi.com/api/images/get?image_id=812"
          },
          {
            "type": 1,
            "label": "picture3_0_7",
            "url": "http://thecatapi.com/api/images/get?image_id=813"
          },
          {
            "type": 1,
            "label": "picture4_0_7",
            "url": "http://thecatapi.com/api/images/get?image_id=814"
          },
          {
            "type": 1,
            "label": "picture5_0_7",
            "url": "http://thecatapi.com/api/images/get?image_id=815"
          },
          {
            "type": 1,
            "label": "picture6_0_7",
            "url": "http://thecatapi.com/api/images/get?image_id=816"
          },
          {
            "type": 1,
            "label": "picture7_0_7",
            "url": "http://thecatapi.com/api/images/get?image_id=817"
          },
          {
            "type": 1,
            "label": "picture8_0_7",
            "url": "http://thecatapi.com/api/images/get?image_id=818"
          }
        ]
        dispatch(loadNodes(nodes.map((node, index) => new NodeDomain(node, index, nodes.length))))
        return nodes
      })
    }
  }
}