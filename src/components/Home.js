import React, { Component } from 'react'
import '../css/node.scss'
import {store} from '../index'
import Api from '../api/Api'
import { browserHistory } from 'react-router'

export default class Home extends Component {

  constructor(props) {
    super(props)
    this.state = {nodes:[]}
    this.node = this.props.params.node ? this.props.params.node : 'root'
  }

  loadNodes() {
    store.dispatch(Api.loadNodes(this.node))
  }

  open(event, node) {
    event.preventDefault()
    const url = node.type === 0 ? `/home/${node.label}` : `/picture?path=${encodeURIComponent(node.url)}`
    browserHistory.push(url);
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.params.node !== this.node){          
      this.node = nextProps.params.node ? nextProps.params.node : 'root';
      this.loadNodes();
    }
  }

  componentWillMount() {
    this.unsubscribe = store.subscribe(() => {
      this.setState({nodes: store.getState()})
    })
  }

  componentWillUnmount() {
    this.unsubscribe();
  }

  componentDidMount() {
    this.loadNodes()
  }

  render() {
    return (
      <nav className='menu'>
        <input className='menu-toggler' id='menu-toggler' type='checkbox' />
        <label htmlFor='menu-toggler'></label>
        <ul>
          {
            this.state.nodes.map(node => <li onClick={(e) => this.open(e, node)}  {...node.toHtml}></li>)
          }
        </ul>
      </nav>
    )
  }
}