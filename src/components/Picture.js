import React, { Component } from 'react'
import '../css/picture.scss'
import {store} from '../index'
import { browserHistory } from 'react-router'
import queryString from 'query-string'

export default class Home extends Component {

  constructor(props) {
    super(props)
    this.state = {
      images: []
    }
    let params = queryString.parse(props.location.search)
    this.picture = decodeURIComponent(params.path)
  }

  componentDidMount() {
    this.loadImages()
  }

  loadImages() {
    this.nodes = store.getState()
    if ( this.nodes === undefined || this.nodes.lenght === 0 ) {
      browserHistory.push('/home')
      return
    }

    let images = []
    this.nodes.forEach(node => {
      if (node.type === 1) {
        if (node.url === this.picture) {
          images.unshift(node)
        }
        else {
          images.push(node)
        }
      }
    })
    this.setState({
      images: images
    })
  }

  render() {
    return (
      <div className="photoset">
        {
          this.state.images.map(image => <a {...image.toPictureHtml}>&nbsp;</a>)
        }
      </div>
    )
  }
}