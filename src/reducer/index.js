export default function reducer(state, action) {
  if (action.type === 'SET_NODES') {
    return action.nodes
  }
  return state
}