import React from 'react'
import ReactDOM from 'react-dom'
import {Router,Route,browserHistory,Redirect} from 'react-router'
import {createStore,applyMiddleware} from 'redux'
import thunkMiddleware from 'redux-thunk'
import reducer from './reducer/index'
import Home from './components/Home'
import Picture from './components/Picture'

export const store = createStore(reducer,applyMiddleware(thunkMiddleware))

ReactDOM.render(
  (
    <Router history={browserHistory}>
      <Redirect exact from="/" to="/home" />
      <Route path="/home(/:node)" component={Home}/>
      <Route path="/picture" component={Picture}/>
    </Router>
  ),
  document.getElementById('root')
);
